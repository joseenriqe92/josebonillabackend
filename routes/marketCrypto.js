const express = require('express');
const router = express.Router();
const controller = require('../controller/marketCrypto');

router.get('/getmarket', async function(req, res, next) {
  res.status(200).send(await controller.getCripto());
});

module.exports = router;
