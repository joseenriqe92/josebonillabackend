const axios = require('axios').default;
const urlPtr = process.env.URLPTR;
const urlCrytop = process.env.URLCRYPTOP;
const urlCoin = process.env.URLCOINT;

const getCripto = async () => {
	try {
		var marketCrypto = [];
		await axios.post(urlPtr, {"coins":["PTR"],"fiats" : ["USD"]})
			.then(({data}) => {
				marketCrypto.push({
					name : 'Petro', 
					symbol : 'PTR',
					price : parseInt(data.data.PTR.USD)
				})
			})
			.catch((err) => {
				console.log('PETRO api is down, puts the value at 60 USD');
				marketCrypto.push({
					name : 'Petro', 
					symbol : 'PTR',
					price : parseInt(process.env.PTR)
				})
			});
			await axios.get(urlCoin).then((res) => {
				marketCrypto.push({
					name : 'Euro', 
					symbol : 'EUR',
					price : res.data.result.value
				})
			});
		const valueAllCrypto = await axios.get(urlCrytop);
		for (let i = 0; i < valueAllCrypto.data.length; i++) {
			marketCrypto.push({
				name : valueAllCrypto.data[i].name, 
				symbol : valueAllCrypto.data[i].symbol,
				price : valueAllCrypto.data[i].price
			})
		}
		marketCrypto.push({
			name : 'Bolivar Soberano',  
			symbol : 'VES',
			price : process.env.VES
		})
		return(marketCrypto)
	} catch (error) {
		return ({code : 500 , message: 'An error has occurred, please try again later.'})
	}
}	

module.exports = {getCripto};
